const { NFC } = require("nfc-pcsc");
const OriginalityVerifier = require('nxp-originality-verifier');

const PCSCAID = Buffer.from('A000000306', 'hex');

const nfc = new NFC();
const originalityVerifier = new OriginalityVerifier();

nfc.on("reader", async reader => {
  async function getSig() {
    try {
      if (reader.name.startsWith("Identiv uTrust")) {
        return Identiv_READ_SIG();
      } else {
        return ACR122U_READ_SIG();
      }
    } catch (err) {
      console.error('READ_SIG error', err);
      return false;
    }
    return true;
  }

  async function Identiv_READ_SIG() {
    // const cmd = Buffer.from([0xFF,0xEF,0x00,0x00,0x05,0x1b,...key]);
    // const response = await reader.transmit(cmd, 4);
    // // Docs say it shoud have the R-APDU (e.g. 9000) at the end, but that is a LIE.
    // // Response is PACK + CRC
    // // https://github.com/nfcpy/nfcpy/blob/master/tests/test_clf_device.py#L109
    // if (response.length === 4) {
    //   console.log('Auth Successful - PACK:', response.slice(0,2).toString('hex'));
    //   return true;
    // } else {
    //   throw new Error(`Auth Failed: ${response.toString('hex')}`);
    // }
    // return false;
  }

  async function ACR122U_READ_SIG() {
    // Explication of command/response at https://github.com/pokusew/nfc-pcsc/issues/36#issuecomment-366800598
    const cmd = Buffer.from([0xFF,0x00,0x00,0x00,0x04,0xd4,0x42,0x3c,0x00]);
    const response = await reader.transmit(cmd, 40);
    if (response[2] == 0x00) {
      return response.slice(3,35).toString('hex');
    } else {
      throw new Error(`Command Failed: ${response.toString('hex')}`);
    }
    
  }

  reader.on("card", async card => {
    let ntag21x = false;
    let type = "";
    let sig = "";
    let sigValid = "";

    const {atr, uid} = card;
    const T0 = atr[1];
    const TD1 = atr[2];
    const TD2 = atr[3];
    const T1 = atr[4];
    const Tk = atr[5];
    const len = atr[6];
    const rid = atr.slice(7, 7+5);
    const std = atr[12];
    const cardName = atr.readUint16BE(13)
    if (rid.equals(PCSCAID) && std === 0x03 /*Standard format: ISO14443A, Part3*/) {
      // console.log('Type 2 tag');
    }

    switch(cardName) {
      case 0x0001: //Mifare 1k
        type = "Mifare 1k";
        break;
      case 0x0002: //Mifare 4k
        type = "Mifare 3k";
        break;
      case 0x0003: //Mifare Ultralight
        type = "Mifare Ultralight (NTAG)";
        ntag21x = true;
        break;
      case 0x0026: //Mifare Mini
        type = "Mifare Mini";
        break;
      case 0xF004: //Topaz and Jewel
        type = "Topaz/Jewel";
        break;
      case 0xF011: // Felica 212k
        type = "Felica 212k";
        break;
      case 0xF012: // Felica 424k
        type = "Felica 424k";
      default:
        type = 'unknown cardName' + cardName;
        break;
    }

    if (ntag21x) {
      try{
        sig = await getSig();

        if (originalityVerifier.verify(uid, sig)) {
          sigValid = "Valid";
        }
    
      } catch (err) {
        sig = err;
      }
    }
    console.log(`Type: ${type}, UID: ${uid}, Sig: ${sig} ${sigValid}`);
  });

  reader.on('card.off', card => {
    // console.log(`${reader.reader.name}  card removed`, card);
  });

  reader.on("error", err => {
    console.error(`an error occurred`, reader, err);
  });

  reader.on("end", () => {
    console.info(`device removed`, reader);
  });
});

nfc.on("error", err => {
  console.error(`an error occurred`, err);
});
