require('dotenv').config()

const crypto = require("crypto");
const ndef = require("ndef");
const { NFC } = require("nfc-pcsc");
const debug = require('debug')('debug')
const arg = require('arg');

const { NDEF_PASSWORD } = process.env

const startingPage = 4;
const pageLength = 4;

const args = arg({
	// Types
	'--help': Boolean,

	'--url': String, // --name <string> or --name=<string>
	'-u': '--url',

	'--password': String, // --name <string> or --name=<string>
	'-p': '--password', // -n <string>; result is stored in --name

	'--key': String, // --name <string> or --name=<string>
	'-k': '--key',

  '--lock': Boolean,
  '--unlock': Boolean,
});

if (args['--help'] || !args['--url']) {
  console.log(
    `Usage: 
    node ${process.argv[1]}
      --url <url including '{uid}' or '{counter}'>
      --password <password> - optional, if included a diversified key will be generated and used, Can also be passed as NDEF_PASSWORD env var
      --key <key> - optional, if included will be used
      --lock - optional, if included card will be locked read only setting key or diversified key (sets PWD and AUTH0 to page 0)
      --unlock - optional, if included card will be unlocked to writes but key will not be updated (sets AUTH0 to FF)
    `
  );
  process.exit(0);
}

const url = args['--url'];
const password = args['--password'] || NDEF_PASSWORD;

const uidAsciiLength = 14;
const counterAsciiLength = 6;
const NOT_FOUND = -1;
const PCSCAID = Buffer.from('A000000306', 'hex');

const ntagStructure = {
  0x12: {
    lastUserPage: 0x27,
    configPageNum: 0x29,
    pwdPageNum: 0x2B,
  },
  0x3e: {
    lastUserPage: 0x81,
    configPageNum: 0x83,
    pwdPageNum: 0x85,
  },
  0x6d: {
    lastUserPage: 0xE1,
    configPageNum: 0xE3,
    pwdPageNum: 0xE5,
  }
};

const nfc = new NFC();

function generateNDEF(url) {
  url = url.replace("{uid}", 'U'.repeat(uidAsciiLength));
  url = url.replace("{counter}", 'C'.repeat(counterAsciiLength));

  const message = [ndef.uriRecord(url)];

  const bytes = Buffer.from(ndef.encodeMessage(message));
  if (bytes.length >= 255) {
    throw new Error("No support for ndef > 255 yet");
  }

  // Maybe do replacement/index of here?
  const buffer = Buffer.alloc(3 + bytes.length);
  buffer[0] = 0x03; // NDEF TLV tag
  buffer[1] = bytes.length;
  bytes.copy(buffer, 2);
  buffer[buffer.length - 1] = 0xfe; // Terminator TLV tag

  let uidMirrorOffset = buffer.indexOf('U'.repeat(uidAsciiLength));
  let counterMirrorOffset = buffer.indexOf('C'.repeat(counterAsciiLength));
  if (uidMirrorOffset !== NOT_FOUND) {
    buffer.fill('0', uidMirrorOffset, uidMirrorOffset + uidAsciiLength);
  }
  if (counterMirrorOffset !== NOT_FOUND) {
    buffer.fill('0', counterMirrorOffset, counterMirrorOffset + counterAsciiLength);
  }

  //NTAG 21x limit the uid/counter mirroring in a number of ways
  if (uidMirrorOffset !== NOT_FOUND && counterMirrorOffset !== NOT_FOUND) {
    if (uidMirrorOffset > counterMirrorOffset) {
      throw new Error(`Counter mirror must come after UID mirror (${uidMirrorOffset} > ${counterMirrorOffset})`);
    }
    if (counterMirrorOffset !== (uidMirrorOffset + uidAsciiLength + 1)) {
      throw new Error(`Counter mirror must be one character after UID mirror end`);
    }
  }

  const results = {
    ndef: buffer,
  }
  if (uidMirrorOffset !== NOT_FOUND) {
    results["uidMirrorOffset"] = uidMirrorOffset;
  }

  if (counterMirrorOffset !== NOT_FOUND) {
    results["counterMirrorOffset"] = counterMirrorOffset;
  }
  return results;
}

nfc.on("reader", async reader => {
  async function read(page, length = 4) {
    // With my uTrust 3700, it returns 4 bytes per packet, not the 16 the library expects
    return await reader.read(page, length, blockSize = 4, packetSize = 4);
  }

  async function write(page, data) {
    return await reader.write(page, data)
  }

  async function writeNDEF(structure,data) {
    const CC = await read(3);
    if (CC[0] != 0xe1) {
      throw new Error("Magic byte for NDEF missing");
    }

    let lastUserPage = structure.lastUserPage;

    for (var page = startingPage; page <= lastUserPage; page++) {
      const start = (page - startingPage) * pageLength;
      const end = Math.min(start + pageLength, data.length);
      const pad = Buffer.alloc(pageLength - (end - start));
      const pageData = Buffer.concat([data.slice(start, end), pad]);
      if (pad.length < pageLength) {
        await write(page, pageData);
      }
    }
  }

  async function readNDEF(structure) {
    const { lastUserPage } = structure

    const TLV = {
      NULL: 0, LockControl: 1, MemoryControl: 2, NDEFMessage: 3, Terminator: 0xfe
    }
    let pageNum = startingPage;
    let index = 0; // The offset within the page
    let ndefFound = false;
    do {
      const page = await read(pageNum);
      switch (page[index++]) {
        case TLV.LockControl:
          index += 1 + page[index];
          break;
        case TLV.NDEFMessage:
          ndefFound = true;
          break;
        default:
          console.log("Buffer didn't start with handled NDEF TLV");
          return Buffer.alloc(0);
      }
      pageNum += Math.floor(index / pageLength);
      index = index % pageLength;
    } while (!ndefFound && pageNum < lastUserPage);

    const page = await read(pageNum);
    const length = page[index];
    if (length === 0xff) {
      throw new Error("No support for ndef > 255 yet");
    }

    const byteCount = Math.ceil(length / pageLength) * pageLength;
    let buffer = Buffer.alloc(0);
    buffer = await read(pageNum, byteCount)
    buffer = buffer.slice(index + 1, index + 1 + length);
    const records = ndef.decodeMessage(buffer);
    return records
      .map(record => {
        switch (record.tnf) {
          case ndef.TNF_WELL_KNOWN:
            switch (record.type) {
              case ndef.RTD_URI:
                return ndef.uri.decodePayload(record.payload);
              default:
                console.log("other type", record.type);
            }
          default:
            console.log("other tnf", record.tnf);
        }
        return null;
      })
      .filter(x => x);
  }

  async function getCardStructure() {
    const CC = await read(3);
    const size = CC[2];
    return ntagStructure[size]
  }

  async function writeMirrors(structure, {uidMirrorOffset, counterMirrorOffset}) {
    const config_0 = await read(structure.configPageNum);

    const MIRROR = config_0[0];
    const MIRROR_PAGE = config_0[2];
    const MIRROR_CONF = (MIRROR & 0xC0) >> 6;
    const MIRROR_BYTE = (MIRROR & 0x30) >> 4;

    let new_mirror_conf = 0x00;
    if (uidMirrorOffset) {
      new_mirror_conf = new_mirror_conf | 0x01
    }
    if (counterMirrorOffset) {
      new_mirror_conf = new_mirror_conf | 0x02;
    }

    const new_mirror_page = uidMirrorOffset ? 4 + Math.floor(uidMirrorOffset / 4) : 0
    const new_mirror_byte = uidMirrorOffset ? uidMirrorOffset % 4 : 0
    const new_mirror = (MIRROR & 0x0F) | (new_mirror_conf << 6) | (new_mirror_byte << 4)
    const new_config_0 = Buffer.from([new_mirror, config_0[1], new_mirror_page, config_0[3]])

    console.log({old: {config_0, MIRROR_PAGE, MIRROR_CONF, MIRROR_BYTE}, new: {new_mirror_conf, new_mirror_page, new_mirror_byte, new_mirror, new_config_0}})
    if (!new_config_0.equals(config_0)) {
      await write(structure.configPageNum, new_config_0);
    }

    if(counterMirrorOffset) {
      const config_1 = await read(structure.configPageNum + 1);
      if((config_1[0] & 0x10) === 0){
        const new_config_1 = Buffer.from([config_1[0] | 0x10, config_1[1], config_1[2], config_1[3]])
        console.log("NFC counter not enabled, enabling ", {old: config_1, new: new_config_1})
        await write(structure.configPageNum + 1, new_config_1);
      }
    }
  }

  async function checkAndAuth(structure, key) {
    const config_0 = await read(structure.configPageNum);
    const AUTH0 = config_0[3];
    if (AUTH0 != 0xFF) {
      console.log("Card write protected, authenticating")
      try {
        if (reader.name.startsWith("Identiv uTrust")) {
          return Identiv_PWD_AUTH(key);
        } else {
          return ACR122U_PWD_AUTH(key);
        }
      } catch (err) {
        console.error('pwd_auth error', err);
        return false;
      }
    }
    return true;
  }

  async function Identiv_PWD_AUTH(key) {
    const cmd = Buffer.from([0xFF,0xEF,0x00,0x00,0x05,0x1b,...key]);
    const response = await reader.transmit(cmd, 4);
    // Docs say it shoud have the R-APDU (e.g. 9000) at the end, but that is a LIE.
    // Response is PACK + CRC
    // https://github.com/nfcpy/nfcpy/blob/master/tests/test_clf_device.py#L109
    if (response.length === 4) {
      console.log('Auth Successful - PACK:', response.slice(0,2).toString('hex'));
      return true;
    } else {
      throw new Error(`Auth Failed: ${response.toString('hex')}`);
    }
    return false;
  }

  async function ACR122U_PWD_AUTH(key) {
    // Explication of command/response at https://github.com/pokusew/nfc-pcsc/issues/36#issuecomment-366800598
    const cmd = Buffer.from([0xFF,0x00,0x00,0x00,0x07,0xd4,0x42,0x1b,...key]);
    const response = await reader.transmit(cmd, 7);
    if (response[2] == 0x00) {
      console.log('Auth Successful - PACK:', response.slice(3,5).toString('hex'));
      return true;
    } else {
      throw new Error(`Auth Failed: ${response.toString('hex')}`);
    }
    return false;
  }

  async function lock(structure, key) {
    // Write PWD
    await write(structure.pwdPageNum, key);

    // Write AUTH0 to lock card
    const config_0 = await read(structure.configPageNum);
    const new_config_0 = Buffer.from([config_0[0], config_0[1], config_0[2], 0x00])
    if (!new_config_0.equals(config_0)) {
      await write(structure.configPageNum, new_config_0);
    }
  }

  async function unlock() {
    const structure = await getCardStructure();
    const config_0 = await read(structure.configPageNum);
    const new_config_0 = Buffer.from([config_0[0], config_0[1], config_0[2], 0xff])
    if (!new_config_0.equals(config_0)) {
      await write(structure.configPageNum, new_config_0);
    }
  }

  reader.on("card", async card => {
    let ntag21x = false;

    const {atr, uid} = card;
    const T0 = atr[1];
    const TD1 = atr[2];
    const TD2 = atr[3];
    const T1 = atr[4];
    const Tk = atr[5];
    const len = atr[6];
    const rid = atr.slice(7, 7+5);
    const std = atr[12];
    const cardName = atr.readUint16BE(13)
    if (rid.equals(PCSCAID) && std === 0x03 /*Standard format: ISO14443A, Part3*/) {
      console.log('Type 2 tag');
    }

    switch(cardName) {
      case 0x0001: //Mifare 1k
        console.log("Mifare 1k");
        break;
      case 0x0002: //Mifare 4k
        console.log("Mifare 3k");
        break;
      case 0x0003: //Mifare Ultralight
        console.log("Mifare Ultralight (NTAG)");
        ntag21x = true;
        break;
      case 0x0026: //Mifare Mini
        console.log("Mifare Mini");
        break;
      case 0xF004: //Topaz and Jewel
        console.log("Topaz/Jewel");
        break;
      case 0xF011: // Felica 212k
        console.log("Felica 212k");
        break;
      case 0xF012: // Felica 424k
        console.log("Felica 424k");
      default:
        console.log('unknown cardName', cardName);
        break;
    }
    console.log("UID:", uid)

    if (!ntag21x) {
      console.log("This tool is for NTAG21x, but the card does not appear to be that");
      process.exit(0);
    }
    
    const structure = await getCardStructure();

    let key;
    if (password) {
      key = crypto.pbkdf2Sync(password, uid, 1000, 4, 'sha512');
      console.log("Diversified Key:", key.toString('hex'));
    }

    if(args['--key']){
      key = Buffer.from(args['--key'], 'hex');
      if (key.length != 4) {
        console.log("Key must be 4 bytes");
        process.exit(0);
      }
      console.log("Key:", key.toString('hex'));
    }

    try {
      const currentNdef = await readNDEF(structure);
      const {ndef, uidMirrorOffset, counterMirrorOffset} = generateNDEF(url);
      if(key) {
        const success = await checkAndAuth(structure, key);
        if (!success) {
          console.log('Authentication failed');
          process.exit(1);
        }
      }
      await writeNDEF(structure, ndef);
      await writeMirrors(structure, {uidMirrorOffset, counterMirrorOffset});
      const newNdef = await readNDEF(structure);
      console.log({ uid, currentNdef, ndef: ndef.toString("hex"), newNdef});

      if(args['--lock']){
        await lock(structure,key);
      }

      if(args['--unlock']){
        await unlock(structure);
      }

      process.exit(0);
    } catch (err) {
      console.error(`error occurred during processing steps`, reader, err);
    }
  });

  reader.on('card.off', card => {
    console.log(`${reader.reader.name}  card removed`, card);
  });

  reader.on("error", err => {
    console.error(`an error occurred`, reader, err);
  });

  reader.on("end", () => {
    console.info(`device removed`, reader);
  });
});

nfc.on("error", err => {
  console.error(`an error occurred`, err);
});
